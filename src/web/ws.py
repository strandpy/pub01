from flask import Flask, flash, request, redirect, url_for, send_from_directory, render_template, session, Response
from graphene import ObjectType, List, Field, String, Float, Int, ID, Schema
from flask_graphql import GraphQLView

app = Flask(__name__)

# https://medium.com/the-andela-way/diving-head-first-into-graphql-part-2-75b3e0562597


class Refinery(ObjectType):
    """Data Object Refinery"""
    id = ID()
    name = String()
    region = Field(lambda: Region)

    def resolve_region(self, info):
        return region_data.get(self.region)


class Region(ObjectType):
    """Data Object Region"""
    id = ID()
    name = String()
    refineries = List(lambda: Refinery)

    def resolve_refineries(self, info):
        return [refinery_data.get(i) for i in self.refineries]


class Period(ObjectType):
    name = String()
    fd = Int()
    td = Int()


class Query(ObjectType):
    """
    Query object is root for queries. Mutators go elsewhere.
    The name is not special, we construct the Schema object with this class later.
    """

    refineries = List(Refinery)
    regions = List(Region)
    period = Field(Period, id=String())

    def resolve_period(self, info, id=None):
        return get_period(id)

    def resolve_refineries(self, info):
        return [tabangao, bukom, pernis]

    def resolve_regions(self, info):
        return [east, europe]


def get_period(id):
    if id:
        return period_data.get(id)
    else:
        return period_data.get("feb")

jan = Period(name="jan", fd=1, td=31)
feb = Period(name="feb", fd=1, td=28)
period_data = {"jan": jan, "feb": feb}

tabangao = Refinery(id="3", name="tabangao", region="25")
bukom = Refinery(id="2", name="bukom", region="25")
pernis = Refinery(id="4", name="pernis", region="24")
refinery_data = {"3": tabangao, "2": bukom, "4": pernis}

east = Region(id="25", name="east", refineries=["3", "2"])
europe = Region(id="24", name="europe", refineries=["4"])
region_data = {"24": europe, "25": east}


# Create a new schema object (to be used later) with the well known end-point query being the Query class from above.
schema = Schema(query=Query)


# Set the graphql schema to be the one we created above.
app.add_url_rule("/graphql", view_func=GraphQLView.as_view("graphql", schema=schema, graphiql=True))

if __name__ == "__main__":
    app.run(port=8000, host='0.0.0.0', debug=True, use_reloader=False)

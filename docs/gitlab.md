## From gitbash
Problem with credential manager

In windows `Control Panel\All Control Panel Items\Credential Manager`

Note this `git credential reject` did not work

This

    $ git config --local credential.helper manager
    $ git push -u origin master
    remote: HTTP Basic: Access denied
    fatal: Authentication failed for 'https://gitlab.com/strandpy/pub01.git/'

was solved with, I think

     git config --local credential.helper wincred

and using the windows control panel

* [super user page](https://superuser.com/questions/1309196/how-to-update-authentication-token-for-a-git-remote)
* [window credential manger](https://support.microsoft.com/en-us/help/4026814/windows-accessing-credential-manager)

`cmdkey` is a windows program and needs `cmdkey //list`, in other words double the slash for windows
